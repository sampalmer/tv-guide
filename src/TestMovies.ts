import { Movie } from "./Movie";
import moment from "moment";

export const testMovies: Movie[] =
[
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-14T15:10:00.000Z"),
      "name": "Novo",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=65340&time=02%3A10%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H50M"),
      "summary": "Graham has total memory loss after an accident. He takes on lovers, but can never remember them and must resort to writing notes on everything. Then one day he recognises his son. A lusty homage to the film Memento. Written and directed by Jean-Pierre Limosin, and stars Eduardo Noriega, Anna Mouglalis and Nathalie Richard.",
      "contentRating": "R",
      "year": 2002,
      "genres": [],
      "imdbRating": 5.6,
      "googleRating": 79
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-14T17:00:00.000Z"),
      "name": "The Hairdresser's Husband",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=718344&time=04%3A00%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H30M"),
      "summary": "As a twelve-year-old, Antoine was introduced to sex by an amply-endowed woman who cut his hair, and as a middle-aged man, he has a passionate romance with a shy hairdresser.",
      "contentRating": "M",
      "genres": [
        "Entertainment"
      ],
      "imdbRating": 7.3,
      "googleRating": 85
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-14T18:30:00.000Z"),
      "name": "Uproar In Heaven",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=725228&time=05%3A30%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H40M"),
      "summary": "The mischievous Monkey King acquires the famed As-You-Will Gold-Banded Cudgel, and goes up against the might of the Jade Emperor.",
      "contentRating": "PG",
      "genres": [
        "Entertainment"
      ],
      "imdbRating": 8.2,
      "googleRating": 86
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-14T20:10:00.000Z"),
      "name": "Kiwi Flyer",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=733002&time=07%3A10%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H35M"),
      "summary": "When 12 year old Ben sets out to win the local trolley derby in memory of his father, he learns what matters in life when he is forced to battle cheating opponents, dodgy loan sharks, and a mother who has banned him from the race.",
      "contentRating": "PG",
      "genres": [
        "Entertainment"
      ],
      "imdbRating": 5.9,
      "googleRating": 88
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-14T21:45:00.000Z"),
      "name": "Mary And Max",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=15047&time=08%3A45%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H40M"),
      "summary": "A tale of friendship between two unlikely pen pals: Mary, a lonely, eight-year-old girl living in the suburbs of Melbourne, and Max, a forty-four-year old, severely obese man living in New York. Nominated for Best Film at the 2009 AFI Awards. Directed by Adam Elliott and narrated by Barry Humphries, and voiced by Philip Seymour Hoffman, Toni Collette and Eric Bana.",
      "contentRating": "PG",
      "year": 2008,
      "genres": [
        "Drama",
        "Comedy"
      ],
      "imdbRating": 8.1,
      "googleRating": 95
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-14T23:25:00.000Z"),
      "name": "Whisky Galore",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=138719&time=10%3A25%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H50M"),
      "summary": "During World War II, the tiny Scottish island of Todday runs out of whisky. When the freighter S.S. Cabinet Minister runs aground nearby during a heavy fog, the islanders are delighted to learn that its cargo consists of 50,000 cases of whisky. When officious English commanding officer Captain Waggett (Eddie Izzard) demands return of the liquor, shopkeeper Joseph Macroon (Gregor Fisher) and his daughters Peggy (Naomi Battrick) and Catriona (Ellie Kendrick) spearhead an island rebellion.",
      "contentRating": "G",
      "year": 1949,
      "genres": [
        "Comedy"
      ],
      "imdbRating": 7.2,
      "googleRating": 86
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-15T01:15:00.000Z"),
      "name": "Walking On Sunshine",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=732879&time=12%3A15%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H50M"),
      "summary": "A bride is unaware that her fiancé and sister were once deeply in love.",
      "contentRating": "PG",
      "genres": [
        "Entertainment"
      ],
      "imdbRating": 5.4,
      "googleRating": 93
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-15T03:05:00.000Z"),
      "name": "The Fox And The Child",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=197582&time=14%3A05%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H40M"),
      "summary": "The story of a magical, life-changing encounter between a wild fox and a young girl, portraying the story of their impossible friendship played out against a breath-taking mountainside wilderness. Springing from his own formative childhood encounters with a fox and drawing on years of wildlife documentary experience seen almost exclusively from the animal's perspective, director Luc Jacquet's film offers an unprecented journey into the magical, secret world of this most elusive and enigmatic creature.",
      "contentRating": "G",
      "year": 2007,
      "genres": [
        "Drama"
      ],
      "imdbRating": 6.9,
      "googleRating": 93
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-15T04:45:00.000Z"),
      "name": "The Piano Tuner Of Earthquakes",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=723654&time=15%3A45%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H50M"),
      "summary": "A mad doctor turns people into mechanical toys. Now he has the lovely opera singer Malvina in his sights. Directed by Timothy Quay and Stephen Quay, and stars Amira Casar and Gottfried John.",
      "contentRating": "PG",
      "genres": [
        "Entertainment"
      ],
      "imdbRating": 6.3
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-15T06:35:00.000Z"),
      "name": "aka The Choir : Boychoir",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=325092&time=17%3A35%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H55M"),
      "summary": "A troubled eleven-year-old boy at a prestigious East Coast music school clashes with the school's demanding choir master (Dustin Hoffman), in this inspiring drama co-starring Kathy Bates, Josh Lucas, Debra Winger, and Glee's Kevin McHale. From acclaimed Canadian director François Girard (Thirty Two Short Films About Glenn Gould, The Red Violin) comes a drama that the entire family can enjoy. Boychoir is the story of a talented youngster struggling against the odds to find his voice.",
      "contentRating": "PG",
      "year": 2015,
      "genres": [
        "Drama"
      ]
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-15T08:30:00.000Z"),
      "name": "The Waiting City",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=221585&time=19%3A30%3A00&fromPage=searcher"),
      "duration": moment.duration("PT2H"),
      "summary": "A young couple make their journey to India to collect their adopted baby, and they discover more than they bargained for. Stars Joel Edgerton.",
      "contentRating": "M",
      "year": 2009,
      "genres": [
        "Drama",
        "Romance"
      ],
      "imdbRating": 6.2
    },
    {
      "channelId": "7TWO",
      "channelName": "Seven Two",
      "start": moment("2019-10-15T09:30:00.000Z"),
      "name": "The Towering Inferno",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=7TWO&showId=202133&time=20%3A30%3A00&fromPage=searcher"),
      "duration": moment.duration("PT3H30M"),
      "summary": "135 floors below the glittering inaugural gala dedicating the world's tallest skyscraper, an electrical fire ignites a conflagration that traps hundreds of partygoers in a Towering Inferno.",
      "contentRating": "PG",
      "year": 1974,
      "genres": [
        "Adventure"
      ],
      "imdbRating": 6.9,
      "googleRating": 89
    },
    {
      "channelId": "7flix",
      "channelName": "7flix",
      "start": moment("2019-10-15T09:30:00.000Z"),
      "name": "As Good As It Gets",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=7flix&showId=881&time=20%3A30%3A00&fromPage=searcher"),
      "duration": moment.duration("PT3H"),
      "summary": "A dysfunctional writer's world is forever changed by a waitress with a sick child, an artist neighbour and an ugly but endearing little dog.",
      "contentRating": "M",
      "year": 2000,
      "genres": [
        "Comedy"
      ]
    },
    {
      "channelId": "GO",
      "channelName": "GO!",
      "start": moment("2019-10-15T09:30:00.000Z"),
      "name": "The Dark Knight",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=GO&showId=14764&time=20%3A30%3A00&fromPage=searcher"),
      "duration": moment.duration("PT3H6M"),
      "summary": "In the blockbuster sequel to Batman Begins, Christian Bale reprises his role as Batman and teams with District Attorney Harvey Dent to face off against Gotham's criminal organisations and the diabolical Joker. (Heath Ledger, Aaron Eckhart)",
      "contentRating": "M",
      "year": 2008,
      "genres": [
        "Drama",
        "Action"
      ],
      "imdbRating": 9,
      "googleRating": 96
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-15T10:30:00.000Z"),
      "name": "Dachra",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=741640&time=21%3A30%3A00&fromPage=searcher"),
      "duration": moment.duration("PT2H5M"),
      "summary": "Set against the backdrop of contemporary Tunisia, the feature revolves around female journalism student Yasmin and two male classmates who set out on a university assignment to solve the cold case of Mongia, a woman found mutilated 25 years ago and now imprisoned in an asylum, suspected of witchcraft.",
      "contentRating": "MA",
      "genres": [
        "Entertainment"
      ],
      "imdbRating": 6.9,
      "googleRating": 93
    },
    {
      "channelId": "SBS2",
      "channelName": "SBS World Movies (formerly SBS Viceland)",
      "start": moment("2019-10-15T12:35:00.000Z"),
      "name": "Novo",
      "ourguideUrl": new URL("https://www.ourguide.com.au/tv_guide_info.php?region=vicregional&state=VIC&date=15102019&channel=SBS2&showId=65340&time=23%3A35%3A00&fromPage=searcher"),
      "duration": moment.duration("PT1H50M"),
      "summary": "Graham has total memory loss after an accident. He takes on lovers, but can never remember them and must resort to writing notes on everything. Then one day he recognises his son. A lusty homage to the film Memento. Written and directed by Jean-Pierre Limosin, and stars Eduardo Noriega, Anna Mouglalis and Nathalie Richard.",
      "contentRating": "R",
      "year": 2002,
      "genres": [],
      "imdbRating": 5.6,
      "googleRating": 79
    }
  ];
  