import { Movie } from "./Movie";
import moment, { Moment } from "moment";

export class MovieRepository {

    constructor(private readonly region: string, private readonly abortController: AbortController){}

    public fetchTodaysBaseMovies = async () => {
        const movies = await this.fetchBaseMovies();
        return movies;
    }
    
    public fetchAllDetails = async (movie: Movie) => {
        await this.addDetails(movie);

        // We need to grab the ratings after the details since the details provide the movie's release year
        await this.addRatings(movie);
    }
    
    private fetchHtml = async (url: string) => {
        const request = new Request(url);
        request.headers.append("Content-Type", "text/html");
    
        const response = await fetch(request, { signal: this.abortController.signal });
        const responseText = await response.text();
        const responseDocument = new DOMParser().parseFromString(responseText, "text/html");
    
        return responseDocument;
    }
    
    /** Gets the list of movies and their basic details */
    private fetchBaseMovies = async () => {
        const basePageUrl = `https://www.ourguide.com.au/searcher.php?region=${encodeURIComponent(this.region)}&type=movie`;
        const ourguideListing = await this.fetchHtml(basePageUrl);
        const movieRows = Array.from(ourguideListing.querySelectorAll("#searchResults tbody tr"));
    
        const allMovies: Movie[] = [];
        let startDate: Moment | null = null;
        for (const row of movieRows) {
            if (row.classList.contains("dateLine")) {
                startDate = moment(row.textContent!.trim(), "dddd Do MMMM", true);
            } else {
                const channelLink = row.querySelector(".channelBox > a");
                const channelHref = channelLink!.attributes.getNamedItem("href");
                const channelUrl = new URL(channelHref!.value, basePageUrl);
                const channelId = channelUrl.searchParams.get("channel");
    
                const channelImageAlt = channelLink!.querySelector("img")!.alt.trim();
                const channelName = /^(.*) Logo$/.exec(channelImageAlt)![1];
    
                const timeText = row.querySelector(".channelTime")!.textContent;
                const nameElement = row.querySelector(".channelDetails .title .result");
                const name = Array.from(nameElement!.childNodes).filter(node => node.nodeType === Node.TEXT_NODE)[0].textContent;
    
                if (!startDate)
                    throw new Error(`Movie listed before date encountered`);
    
                const startTime = moment(timeText!.trim(), "h:mm a", true);
                const start = moment(new Date(startDate.year(), startDate.month(), startDate.date(), startTime.hour(), startTime.minute()));
    
                const ourguideUrlAttribute = nameElement!.attributes.getNamedItem("data");
                const ourguideUrl = new URL(ourguideUrlAttribute!.value);
    
                allMovies.push({
                    channelId,
                    channelName,
                    start: start,
                    name: name!.trim(),
                    ourguideUrl,
                } as Movie);
            }
        }
    
        const now = moment();
        const tomorrow = moment().add(1, "day").startOf("day")
    
        const todaysMovies = allMovies.filter(movie => movie.start.isSameOrAfter(now) && movie.start.isBefore(tomorrow));
        return todaysMovies;
    }

    private addDetails = async (movie: Movie) => {
        const detailPage = await this.fetchHtml(movie.ourguideUrl.href);
    
        // Duration
        const startText = detailPage.querySelector(".tvShow > .para")!.textContent!.trim();
        const startMatch = /^.*\((\d+) minutes\)$/.exec(startText!);
        const minutesText = startMatch![1];
        const minutes = parseInt(minutesText, 10);
        movie.duration = moment.duration(minutes, "minutes");
    
        // Summary
        movie.summary = detailPage.querySelectorAll(".tvShow .para")[1].textContent!.trim();
    
        // Content rating
        movie.contentRating = detailPage.querySelector(".tvShow .ratingImg")!.textContent!.trim();
    
        // Year
        const tagsLine = detailPage.querySelector(".tvShow .ratingDesc > div:first-child")!.textContent!.trim();
        const tags = tagsLine.split(/,\s*/);
        const yearPattern = /\d{4}/;
        const yearTag = tags.filter(tag => yearPattern.test(tag))[0];
        if (yearTag)
            movie.year = parseInt(yearTag, 10);
        
        // Genres
        const tagBlacklist = ["CC", "HD", "Repeat", "Premiere", "USA", "United States", "Greece", "United Kingdom", "UK", "Italy", "France", "Brazil", "South Korea", ""];
        movie.genres = tags.filter(tag => !yearPattern.test(tag) && !tagBlacklist.includes(tag));
    }
    
    private addRatings = async (movie: Movie) => {
        const movieNames = [
            // First try the movie name as provided by OurGuide
            movie.name,
        ];
        if (movie.channelId === "7MATE" && movie.name.length === 36) {
            // OurGuide truncates 7mate movies to 36 characters.
            // Google doesn't pick up the movie if we use a truncated word in the title, so we'll remove the last word entirely.
            const lastWordIndex = movie.name.lastIndexOf(" ");
            if (lastWordIndex > -1) {
                const newName = movie.name.substring(0, lastWordIndex);
                movieNames.push(newName);
            }
        }

        const movieSuffixes = [
            // First try with the "Movie" suffix so we don't end up getting a non-movie result
            "Movie",

            // But if that doesn't work then maybe the movie is part of a series (eg a trilogy), in which case the "Movie" suffix picks up the entire series rather than the first release, so we'll try without the suffix
            null,
        ];

        let movieYears: (number | null)[];
        if (movie.year) {
            // Sometimes a movie is released in different years depending on the country.
            // So the year that Google uses might be off by one compared to OurGuide.
            // For example, the movie Agora (2009)
            movieYears = [
                movie.year,
                movie.year - 1,
                movie.year + 1,
                movie.year - 2,
                movie.year + 2,
                null, // Google doesn't know the year of some movies, such as Nightmare Tenant
            ]
        } else {
            movieYears = [null];
        }

        const searchQueries: string[] = [];
        for (const year of movieYears)
            for (const suffix of movieSuffixes)
                for (const name of movieNames) {
                    const parts = [name, suffix, year];
                    const searchQuery = parts
                        .filter(part => part)
                        .join(" ");
                    searchQueries.push(searchQuery);
                }
        
        let knowledgePanel: KnowledgePanel | null = null;
        for (const searchQuery of searchQueries) {
            const googleSearchResult = await this.googleSearch(searchQuery);

            // The knowledge panel sometimes comes across in different formats
            const possibleKnowledgePanel = googleSearchResult.querySelector(".knowledge-panel, .kp-wholepage");

            if (possibleKnowledgePanel) {
                const ratingContainers = Array.from(possibleKnowledgePanel.querySelectorAll(".NY3LVe"));
                const googleRatingElement = possibleKnowledgePanel.querySelector(".Vrkhme");

                // Sometimes a search finds a knowledge panel that's not the movie type: https://www.google.com/search?q=descendants+2+movie
                if (ratingContainers.length > 0 || googleRatingElement) {
                    knowledgePanel = {
                        ownerDocument: possibleKnowledgePanel.ownerDocument!,
                        ratingContainers,
                        googleRatingElement,
                    };
                    break;
                }
            } else {
                console.warn(`Couldn't find Google knowledge panel for query "${searchQuery}"`, {
                    movie,
                    search: {
                        query: searchQuery,
                        result: googleSearchResult.body.outerHTML
                    }
                });
            }
        }
        
        if (knowledgePanel) {
            if (knowledgePanel.ratingContainers.length > 0) {
                const imdbRatingContainer = knowledgePanel.ratingContainers.filter(c => c.querySelector(".wDgjf")!.textContent === "IMDb")[0];
                if (imdbRatingContainer) {
                    const possibleRating = imdbRatingContainer.querySelector(".IZACzd")!.textContent;
                    
                    if (possibleRating) {
                        const matches = /(\d(?:\.\d)?)\/10/.exec(possibleRating);
                        if (matches) {
                            const ratingText = matches[1];
                            const rating = parseFloat(ratingText);
                            if (!isNaN(rating))
                                movie.imdbRating = rating;
                        }
                    }
                }
            }

            if (knowledgePanel.googleRatingElement) {
                const childNodes = Array.from(knowledgePanel.googleRatingElement!.childNodes);
                const possibleRating = childNodes.filter(node => node.nodeType === Node.TEXT_NODE)[0].textContent;

                if (possibleRating) {
                    const matches = /(\d+)%/.exec(possibleRating);
                    if (matches) {
                        const ratingText = matches[1];
                        const rating = parseInt(ratingText, 10);
                        if (!isNaN(rating))
                            movie.googleRating = rating;
                    }
                }
            }

            // TODO: If the Google rating element was missing, then retry the search. Occasionally, Google doesn't show the rating and instead just asks the user to rate the movie.

            let missingRatingProviders = [];
            if (movie.imdbRating === undefined)
                missingRatingProviders.push("IMDb");
            if (movie.googleRating === undefined)
                missingRatingProviders.push("Google");
            
            if (missingRatingProviders.length > 0) {
                console.warn(`Couldn't find ${missingRatingProviders.join(" and ")} user rating${missingRatingProviders.length === 1 ? "" : "s"} for movie ${movie.name}`, {
                    movie,
                    search: {
                        result: knowledgePanel.ownerDocument!.body.outerHTML
                    }
                });
            }
        }
    }

    private googleSearch = async (googleSearchQuery: string) => {
        const googleSearchUrl = `/search?q=${encodeURIComponent(googleSearchQuery)}`;
        const googleSearchResult = await this.fetchHtml(googleSearchUrl);
        return googleSearchResult;
    }
}

interface KnowledgePanel {
    ownerDocument: Document;
    ratingContainers: Element[];
    googleRatingElement: Element | null;
}