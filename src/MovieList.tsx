import React from "react";
import { Movie } from "./Movie";
import { MovieRepository } from "./MovieRepository";
import { Theme, withStyles } from "@material-ui/core/styles";
import { WithStyles } from "@material-ui/styles/withStyles";
import LinearProgress from '@material-ui/core/LinearProgress';
import MUIDataTable, { MUIDataTableColumnOptions } from "mui-datatables";
import moment, { Moment } from "moment";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import { testMovies } from "./TestMovies";
import MomentUtils from '@date-io/moment';
import {
  MuiPickersUtilsProvider,
  KeyboardDateTimePicker,
} from '@material-ui/pickers';
import Grid from "@material-ui/core/Grid";
import Select from "@material-ui/core/Select";
import { MenuItem, Checkbox, ListItemText, Input, InputLabel } from "@material-ui/core";

const testMode = false;
const dateTimeFormat = "DD/MM/YYYY HH:mm";

interface Props extends WithStyles<typeof styles> {
  region: string;
};

type State = {
  movies?: Movie[];
  moviesLoaded: number;
};

const styles = (theme: Theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  inline: {
    display: 'inline',
  },
});

class MovieList extends React.Component<Props, State> {
  private readonly abortController: AbortController;

  constructor(props: Props) {
    super(props);

    this.state = {
      movies: undefined,
      moviesLoaded: 0
    };

    this.abortController = new AbortController();
  }

  async componentDidMount() {
    if (testMode) {
      this.setState({
        movies: testMovies,
        moviesLoaded: testMovies.length,
      });

      return;
    }

    const movieRepository = new MovieRepository(this.props.region, this.abortController);

    let movies: Movie[];
    try {
      movies = await movieRepository.fetchTodaysBaseMovies();
      this.setState({
        movies,
        moviesLoaded: 0
      });

      const fetchMovieDetails = movies.map(async movie => {
        await movieRepository.fetchAllDetails(movie);
        this.setState(state => ({
          moviesLoaded: state.moviesLoaded + 1
        }))
      });
      await Promise.all(fetchMovieDetails);
    } catch (e) {
      if (e instanceof DOMException && e.name === "AbortError")
        return; // The op was aborted, which means we're unmounting, so we can safely ignore
      else
        throw e;
    }
  }

  componentWillUnmount() {
    this.abortController.abort();
  }

  render() {
    const classes = this.props.classes;
    const allGenres = this.state.movies && Array.from(new Set(this.state.movies.map(movie => movie.genres).flat())).sort();

    return (
      <div className="movie-list">
        {!this.state.movies || this.state.moviesLoaded < this.state.movies.length ? (
          <LinearProgress variant={this.state.movies ? "determinate" : "indeterminate"} value={this.state.movies ? this.state.moviesLoaded / this.state.movies.length * 100 : undefined} />
        ) : (
          <MUIDataTable
            title="Movies"
            data={this.state.movies.map(movie => ({
              ...movie,

              // MUI-Datatables doesn't allow access to the original object while rendering cell data, so we need to do complex mapping up-front
              description: <>
                <a
                    rel="noopener noreferrer"
                    target="_blank"
                    href={`https://www.youtube.com/results?search_query=${encodeURIComponent(`${movie.name} ${movie.year || ''} Movie Trailer`)}`}
                >{formatMovieLine1(movie)}</a>
                <br/>
                <Typography
                  component="span"
                  variant="body2"
                  className={classes.inline}
                  color="textPrimary"
                >
                  {movie.summary}
                </Typography>
                <Typography
                  component="span"
                  variant="body2"
                  color="textSecondary"
                  >
                  {` (${movie.channelName})`}
                </Typography>
              </>,
              ratings: <Typography noWrap={true}>
                {movie.imdbRating && `IMDb: ${movie.imdbRating}/10`}
                <br />
                {movie.googleRating && `Google: ${movie.googleRating}%`}

              </Typography>
            }))}
            columns={[
              {
                name: "start",
                label: "Start",
                options: {
                  customBodyRender: (value: Moment) => <Typography noWrap={true}>{value.format("hh:mm A")}</Typography>,
                  filterType: "custom",
                  customFilterListRender: (value: string[]) => value[0] ? `Starts from ${moment(value[0]).format(dateTimeFormat)}` : false,
                  filterOptions: {
                    display: (filterList, onChange, index, column) =>
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                      <Grid container justify="space-around">
                        <KeyboardDateTimePicker
                          minDate={this.state.movies!.map(m => m.start).sort((a, b) => a.isBefore(b) ? -1 : a.isSame(b) ? 0 : 1)[0]}
                          maxDate={this.state.movies!.map(m => m.start).sort((a, b) => a.isBefore(b) ? 1 : a.isSame(b) ? 0 : -1)[0]}
                          variant="inline"
                          format={dateTimeFormat}
                          fullWidth
                          label="Start (min)"
                          value={filterList[index][0] || null}
                          onChange={event => {
                            (filterList[index] as any)[0] = event ? event.toISOString() : "";
                            onChange(filterList[index], index, column);
                          }}
                          KeyboardButtonProps={{
                            'aria-label': 'change date & time',
                          }}
                        />
                      </Grid>
                    </MuiPickersUtilsProvider>,
                    logic: (prop, filterValues) => filterValues[0] && (prop as unknown as Moment).isBefore(filterValues[0])
                  }
                } as MUIDataTableColumnOptions
              },
              {
                name: "description",
                label: "Description",
                options: {
                  filter: false
                }
              },
              {
                name: "ratings",
                label: "Ratings",
                options: {
                  filter: false
                }
              },
              {
                name: "year",
                label: "Year",
                options: {
                  display: "excluded",
                  filterType: "custom",
                  filterList: ["1980"],
                  customFilterListRender: (value: string[]) => value[0] ? `Year: ${value[0]}+` : false,
                  filterOptions: {
                    display: (filterList, onChange, index, column) =>
                      <TextField
                        fullWidth
                        label="Year (min)"
                        inputProps={{
                          min: 1850,
                          max: new Date().getFullYear(),
                          step: 5,
                        }}
                        value={filterList[index].toString() || ''}
                        onChange={event => {
                          (filterList[index] as any)[0] = event.target.value;
                          onChange(filterList[index], index, column);
                        }}
                        type="number"
                      />,
                    logic: (prop, filterValues) => (prop as unknown as number) < parseFloat(filterValues[0])
                  }
                } as MUIDataTableColumnOptions
              },
              {
                name: "genres",
                label: "Genres",
                options: {
                  display: "excluded",
                  filterType: "custom",
                  filterList: allGenres!.filter(genre => genre !== "Horror"),
                  customFilterListRender: (value: string[]) => formatGenreFilters(allGenres!, value),
                  filterOptions: {
                    display: (filterList, onChange, index, column) =>
                      <>
                        <InputLabel htmlFor={column.name}>{column.label}</InputLabel>
                        <Select
                          multiple
                          fullWidth
                          value={filterList[index] || []}
                          renderValue={selected => (selected as string[]).join(', ')}
                          name={column.name}
                          onChange={event => {
                            (filterList as any)[index] = event.target.value || [];
                            onChange(filterList[index], index, column);
                          }}
                          input={<Input name={column.name} id={column.name} />}>
                        >
                          {allGenres!.map(genre => (
                            <MenuItem key={genre} value={genre}>
                              <Checkbox
                                checked={filterList[index].indexOf(genre) > -1}
                                value={genre}
                              />
                              <ListItemText primary={genre} />
                            </MenuItem>
                          ))}
                        </Select>
                      </>,
                    logic: ((movieGenres: string[], genreWhitelist: string[]) => movieGenres.some(movieGenre => !genreWhitelist.includes(movieGenre))) as any,
                  }
                } as MUIDataTableColumnOptions
              },
              {
                name: "imdbRating",
                label: "IMDb",
                options: {
                  display: "excluded",
                  filterType: "custom",
                  filterList: ["6.5"],
                  customFilterListRender: (value: string[]) => value[0] ? `IMDb: ${value[0]}+` : false,
                  filterOptions: {
                    display: (filterList, onChange, index, column) =>
                      <TextField
                        fullWidth
                        label="IMDb Rating"
                        inputProps={{
                          min: 0,
                          max: 10,
                          step: .5,
                        }}
                        value={filterList[index].toString() || ''}
                        onChange={event => {
                          (filterList[index] as any)[0] = event.target.value;
                          onChange(filterList[index], index, column);
                        }}
                        type="number"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">/10</InputAdornment>
                        }}
                      />,
                    logic: (prop, filterValues) => (prop as unknown as number) < parseFloat(filterValues[0])
                  }
                } as MUIDataTableColumnOptions
              },
              {
                name: "googleRating",
                label: "Google",
                options: {
                  display: "excluded",
                  filterType: "custom",
                  filterList: ["85"],
                  customFilterListRender: (value: string[]) => value[0] ? `Google: ${value[0]}%+` : false,
                  filterOptions: {
                    display: (filterList, onChange, index, column) =>
                      <TextField
                        fullWidth
                        label="Google Rating"
                        inputProps={{
                          min: 0,
                          max: 100,
                          step: 5,
                        }}
                        value={filterList[index].toString() || ''}
                        onChange={event => {
                          (filterList[index] as any)[0] = event.target.value;
                          onChange(filterList[index], index, column);
                        }}
                        type="number"
                        InputProps={{
                          endAdornment: <InputAdornment position="end">%</InputAdornment>
                        }}
                      />,
                    logic: (prop, filterValues) => (prop as unknown as number) < parseInt(filterValues[0], 10)
                  }
                } as MUIDataTableColumnOptions
              },
            ]}
            options={{
              pagination: false,
              selectableRows: "none",
              print: false,
              download: false,
              sort: false,
              search: false,
              customFooter: rowcount =>
              <Grid
                container
                alignItems="center"
                justify="center"
              >
                {`${rowcount} movies`}
              </Grid>,
            }}
          />
          )}
      </div>
    );
  }
}

export default withStyles(styles)(MovieList);

function formatMovieLine1(movie: Movie) {
  let result = movie.name;

  const tags = [
    movie.year,
    movie.genres.join(", "),
    movie.contentRating,
    movie.duration.format("h[h] m[m]"),    
  ];

  result += tags
    .filter(tag => tag)
    .map(tag => ` (${tag})`)
    .join(' ');

  return result;
}

function formatGenreFilters(allGenres: string[], value: string | string[]) {
  // Sometimes MUI Datatables provides a string value instead of an array of string. No idea why.
  if (!(value instanceof Array))
    return value;
  
  const notAllGenres = value.length !== allGenres.length && allGenres.some(genre => !value.includes(genre));
  if (notAllGenres) {
    const result = value.join(', ');
    return result;
  }

  return false;
}
