const proxy = require('http-proxy-middleware');

// Google doesn't allow CORS, so we need to proxy requests to it to bypass browser CORS restrictions
module.exports = function(app) {
  app.use('/search', proxy({
    target: 'https://www.google.com',
    changeOrigin: true,
  }));
};
