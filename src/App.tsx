import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ThemeProvider } from '@material-ui/styles';
import theme from './theme';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import { Copyright } from './Copyright';
import { TitledMovieList } from './TitledMovieList';

const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />

      <Container maxWidth="md">
        <Box my={4}>
          <TitledMovieList region="sydney" />
          <Copyright />
        </Box>
      </Container>

    </ThemeProvider>
  );
}

export default App;
