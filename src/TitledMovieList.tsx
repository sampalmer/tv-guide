import React from "react"
import Typography from "@material-ui/core/Typography"
import MovieList from "./MovieList";

export function TitledMovieList(props: { region: string }) {
    return (
        <>
            <Typography variant="h4" component="h1" gutterBottom>
                Today's movies for {props.region}
            </Typography>
            <MovieList region={props.region} />
        </>
    );
}
