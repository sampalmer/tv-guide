import { Moment, Duration } from "moment";

export type Movie = {
  ourguideUrl: URL;
  channelId: string;
  channelName: string;
  start: Moment;
  year?: number;
  name: string;
  summary: string;
  duration: Duration;
  contentRating: string;

  /** Rating out of 10 */
  imdbRating?: number;

  /** Rating as a percentage (eg out of 100) */
  googleRating?: number;

  genres: string[];
};
